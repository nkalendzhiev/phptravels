Feature: Frontend

  Scenario: Validate backend change and book room

    Given User open frontend
    And User navigates to login
    And User logs in "frontend"
    When User navigates to Hotels in "frontend"
    And User selects edited hotel
    Then User verifies room "Triple Rooms" has price "$1,234"
    And User submits booking for dates "25/12/2018" and "26/12/2018"