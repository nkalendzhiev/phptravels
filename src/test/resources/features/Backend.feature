Feature: Backend

  Scenario: Edit room price

    Given User open backend
    And User logs in "backend"
    When User navigates to Hotels in "backend"
    And User navigates to Rooms
    And User selects to edit first list item
    When User change price to "1234"
    And User saves changes
    Then User log out