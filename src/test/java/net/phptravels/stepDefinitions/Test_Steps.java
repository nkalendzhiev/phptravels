package net.phptravels.stepDefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.phptravels.pageobjects.BackendPO;
import net.phptravels.pageobjects.FrontendPO;
import net.phptravels.tools.Tools;
import org.junit.Assert;
import org.openqa.selenium.*;

import java.util.List;

import static net.phptravels.tools.SeleniumWebDriver.close;


public class Test_Steps extends Tools {

    String backend_email = "admin@phptravels.com";
    String backend_password = "demoadmin";
    String frontend_email = "user@phptravels.com";
    String frontend_password = "demouser";

    @Given("^User open backend$")
    public void user_open_backend() throws Throwable {

        buildDriver();
    }

    @And("^User navigates to Rooms$")
    public void user_navigates_to_Rooms() throws Throwable {

        click(BackendPO.getRoomsMenu());
    }

    @And("^User selects to edit first list item$")
    public void user_selects_edit_first_list_item() throws Throwable {

        click(BackendPO.getEditButton());

        //Not ideal, but still verifies the correct room will be edited
        assertTextPresent("Triple Rooms");
        assertTextPresent("Rendezvous Hotels");
    }

    @When("^User change price to \"([^\"]*)\"$")
    public void user_change_price_to(String arg1) throws Throwable {

        click(BackendPO.getPrice());
        clear(BackendPO.getPrice());
        sendKeys(BackendPO.getPrice(), arg1);
    }

    @And("^User saves changes$")
    public void user_saves_changes() throws Throwable {

        click(BackendPO.getSubmitButton());
    }

    @Then("^User log out$")
    public void user_log_out() throws Throwable {

        click(BackendPO.getLogOutButton());

        assertTextPresent("Login");
    }

    @Given("^User open frontend$")
    public void user_open_frontend() throws Throwable {

        getWebDriver().get("https://www.phptravels.net/");
    }

    @And("^User navigates to login$")
    public void user_navigates_to_login() throws Throwable {

        click(FrontendPO.getMyAccount());
        click(FrontendPO.getLogInMenu());
    }

    @And("^User logs in \"([^\"]*)\"$")
    public void user_logs_in(String arg1) throws Throwable {

        if (arg1.equals("frontend")) {

            click(FrontendPO.getEmail());
            clear(FrontendPO.getEmail());
            sendKeys(FrontendPO.getEmail(), frontend_email);
            click(FrontendPO.getPassword());
            clear(FrontendPO.getPassword());
            sendKeys(FrontendPO.getPassword(), frontend_password);

            click(FrontendPO.getLogInButton());

            assertTextPresent("Hi, Johny Smith");
        }
        else {

            click(BackendPO.getEmail());
            clear(BackendPO.getEmail());
            sendKeys(BackendPO.getEmail(), backend_email);
            click(BackendPO.getPassword());
            clear(BackendPO.getPassword());
            sendKeys(BackendPO.getPassword(), backend_password);

            click(BackendPO.getLogInButton());

            assertTextPresent("Booking Summary");
        }
    }

    @When("^User navigates to Hotels in \"([^\"]*)\"$")
    public void user_navigates_to_Hotels_in(String arg1) throws Throwable {

        if (arg1.equals("frontend")) {

            click(FrontendPO.getHotelsMenu());
        }
        else {

            click(BackendPO.getHotelsMenu());
        }
    }

    @And("^User selects edited hotel$")
    public void user_selects_edited_hotel() throws Throwable {

        click(FrontendPO.getHotel());
    }

    @Then("^User verifies room \"([^\"]*)\" has price \"([^\"]*)\"$")
    public void user_verifies_room_price_is(String arg1, String arg2) throws Throwable {

        WebDriver driver = Tools.getWebDriver();

        List<WebElement> roomsList = driver.findElements(By.xpath("/html/body/div[4]/div[4]/section/div/table/tbody/tr"));
        //I am iterating throug all room results to match a room title with its price and manually assert
        //so the test desn't fail if row doesn't match search parameters
        for (WebElement element: roomsList) {

            WebElement title = element.findElement(By.xpath("td[1]/div[2]/div[1]/div[1]/h4[1]/a[1]/b[1]"));

            WebElement price = element.findElement(By.xpath("td[1]/div[2]/div[2]/div[1]/div[2]/h2[1]/strong[1]"));

            if (title.getAttribute("innerHTML").equals(arg1) && price.getAttribute("innerHTML").equals(arg2)) {
                Assert.assertTrue("Correct Price found", true);
                return;
            }
        }
        Assert.assertTrue("Wrong Price found", false);
    }

    @And("^User submits booking for dates \"([^\"]*)\" and \"([^\"]*)\"$")
    public void user_submits_booking_for_dates_and(String arg1, String arg2) throws Throwable {

        click(FrontendPO.getCheckIn());
        clear(FrontendPO.getCheckIn());
        sendKeys(FrontendPO.getCheckIn(), arg1);
        click(FrontendPO.getCheckOut());
        clear(FrontendPO.getCheckOut());
        sendKeys(FrontendPO.getCheckOut(), arg2);
        click(FrontendPO.getModifyButton());

        JavascriptExecutor js = (JavascriptExecutor) getWebDriver();
        WebElement element = getWebDriver().findElement(By.xpath("//tbody//tr[1]//td[1]//div[2]//div[2]//div[1]//div[3]//div[1]//label[1]//div[1]"));
        js.executeScript("arguments[0].scrollIntoView();", element);

        click(FrontendPO.getRoomCheckbox());

        click(FrontendPO.getBookNowButton());

        click(FrontendPO.getConfirmBookingButton());

        assertTextPresent("Unpaid");
    }

    @After
    public void tearDown(Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
            close();
        }
    }
}
