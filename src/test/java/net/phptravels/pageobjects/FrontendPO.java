package net.phptravels.pageobjects;

import net.phptravels.tools.Tools;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class FrontendPO extends Tools {

    static synchronized public WebElement getMyAccount() { return findElementBy(By.xpath("//ul[@class='nav navbar-nav navbar-right hidden-sm go-left']//li[@id='li_myaccount']//a[@class='dropdown-toggle go-text-right']"));}

    static synchronized public WebElement getLogInMenu() { return findElementBy(By.xpath("//nav[@class='navbar navbar-default']//ul[@class='dropdown-menu']//li[1]//a[1]"));}

    static synchronized public WebElement getEmail() { return findElementBy(By.xpath("//input[@placeholder='Email']"));}

    static synchronized public WebElement getPassword() { return findElementBy(By.xpath("//input[@placeholder='Password']"));}

    static synchronized public WebElement getLogInButton() { return findElementBy(By.xpath("//button[@type='submit']"));}

    static synchronized public WebElement getHotelsMenu() { return findElementBy(By.xpath("//a[@title='Hotels']"));}

    static synchronized public WebElement getHotel() { return findElementBy(By.xpath("//b[contains(text(),'Rendezvous Hotels')]"));}

    static synchronized public WebElement getRoomCheckbox() { return findElementBy(By.xpath("//tbody//tr[1]//td[1]//div[2]//div[2]//div[1]//div[3]//div[1]//label[1]//div[1]"));}

    static synchronized public WebElement getBookNowButton() { return findElementBy(By.xpath("//div[@class='panel panel-default']//button[@type='submit']"));}

    static synchronized public WebElement getConfirmBookingButton() { return findElementBy(By.xpath("//button[@name='logged']"));}

    static synchronized public WebElement getCheckIn() { return findElementBy(By.xpath("//input[@placeholder='Check in']"));}

    static synchronized public WebElement getCheckOut() { return findElementBy(By.xpath("//input[@placeholder='Check out']"));}

    static synchronized public WebElement getModifyButton() { return findElementBy(By.xpath("//input[@value='Modify']"));}

}
