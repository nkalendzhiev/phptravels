package net.phptravels.pageobjects;

import net.phptravels.tools.Tools;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class BackendPO extends Tools{

    static synchronized public WebElement getEmail() { return findElementBy(By.xpath("//input[@placeholder='Email']"));}

    static synchronized public WebElement getPassword() { return findElementBy(By.xpath("//input[@placeholder='Password']"));}

    static synchronized public WebElement getLogInButton() { return findElementBy(By.xpath("//button[@type='submit']"));}

    static synchronized public WebElement getHotelsMenu() { return findElementBy(By.xpath("//a[@href='#Hotels']"));}

    static synchronized public WebElement getRoomsMenu() { return findElementBy(By.xpath("//a[contains(text(),'Rooms')]"));}

    static synchronized public WebElement getEditButton() { return findElementBy(By.xpath("//tbody//tr[1]//td[11]//span[1]//a[1]//i[1]"));}

    static synchronized public WebElement getPrice() { return findElementBy(By.xpath("//input[@placeholder='Price']"));}

    static synchronized public WebElement getSubmitButton() { return findElementBy(By.xpath("//button[@id='update']"));}

    static synchronized public WebElement getLogOutButton() { return findElementBy(By.xpath("//a[@class='btn btn-danger btn-sm btn-block']"));}



}
