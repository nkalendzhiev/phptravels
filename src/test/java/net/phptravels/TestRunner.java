package net.phptravels;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(strict = true,
        features = {"./src/test/resources/features"},
//        format = { "pretty"},
        plugin = { "pretty", "html:target/cucumber-reports" },
        monochrome = true,
        glue = { "net.phptravels.stepDefinitions" }
)
public class TestRunner {
}
