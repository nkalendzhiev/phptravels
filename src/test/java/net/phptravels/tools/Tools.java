package net.phptravels.tools;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Tools {

    public static String envUrl = System.getProperty("envUrl");

    static {
        if (System.getProperty("envUrl") == null)
            envUrl = "https://www.phptravels.net/admin";
    }

    static synchronized public void buildDriver() {
        SeleniumWebDriver.setWebDriver();
        SeleniumWebDriver.getWebDriver().get(envUrl);
    }

    static synchronized public WebDriver getWebDriver() {
        return SeleniumWebDriver.getWebDriver();
    }

    static synchronized public WebElement findElementBy(By by) {
        WebElement element = null;
        int br=1;
        while(br<=5) {
            try {
                br++;
                SeleniumWebDriver.getWebDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
                WebDriverWait wait = new WebDriverWait(SeleniumWebDriver.getWebDriver(), 2);
                element = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
                SeleniumWebDriver.getWebDriver().manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
                return element;
            } catch (Exception e) {
                if (br==5) {
                    throw e;
                }
                try {Thread.sleep(1000);} catch (Exception exc) {}
            }
        }
        return element;
    }

    static public void click(WebElement element) {
        doAction(element,"click","");
    }

    static public void clear(WebElement element) {
        doAction(element,"clear","");
    }


    static public void sendKeys(WebElement element,String keys) {
        doAction(element,"sendKeys",keys);
    }

    static private String doAction(WebElement element, String action, String text){
        int br = 0;
        String result = null;
        while (br<=5) {
            br++;
            try {
                switch (action){
                    case "click": element.click();break;
                    case "clear": element.clear();break;
                    case "sendKeys": element.sendKeys(text);break;
                    case "getAttribute": result = element.getAttribute(text);break;
                    default: Assert.assertTrue("There is no such Action",false);
                }
                br=6;
            }
            catch (Exception e) {
                if (br==5) {
                    throw e;
                }
                try {Thread.sleep(1000);} catch (Exception exc) {}
            }
        }
        return result;
    }

    public boolean verifyTextPresent(final String text) {
        boolean isPresent = false;
        int br=0;
        while (!isPresent&&br<=15) {
            try {
                br++;
                isPresent = SeleniumWebDriver.getWebDriver().getPageSource().contains(text);

                if (isPresent) return true;
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return false;
    }

    public void assertTextPresent(String text) {
        Assert.assertTrue("The text '"+text+"' is not present on the page, please check!"
                ,verifyTextPresent(text));
    }

    static public void takeScreenshot() throws IOException {
        DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy h-m-s");
        Date date = new Date();
        File scrFile = ((TakesScreenshot)getWebDriver()).getScreenshotAs(OutputType.FILE);
        String FILE_PATH = "IntelJ_Screenshots/";
        String FILE_EXTENSION = ".png";
        FileUtils.copyFile(scrFile, new File(FILE_PATH + dateFormat.format(date) + FILE_EXTENSION));

    }

}
