package net.phptravels.tools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class SeleniumWebDriver {

    static private ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>() {

        @Override
        public synchronized WebDriver initialValue() {
            System.setProperty("webdriver.chrome.driver", "/Users/nikolaykalendzhiev/selenium-java-3/chromedriver");
            return new ChromeDriver();
        }
    };

    static public synchronized WebDriver getWebDriver() {
        return webDriver.get();
    }

    static public synchronized WebDriver setWebDriver() {
        System.setProperty("webdriver.chrome.driver", "/Users/nikolaykalendzhiev/selenium-java-3/chromedriver");
        WebDriver webDriver2 = new ChromeDriver();
        webDriver.set(webDriver2);
        return webDriver2;
    }

    static public synchronized void close() {
        webDriver.get().quit();
    }
}